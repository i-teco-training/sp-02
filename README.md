#### 1.ТРЕБОВАНИЯ К SOFTWARE.
Windows 10.

Java SE Runtime Environment 8.

#### 2. СТЕК ТЕХНОЛОГИЙ.
Java SE 1.8.

Фреймворк Apache Maven 3.6.0.

Spring Framework.

#### 3. РАЗРАБОТЧИК.
Алексеев Андрей.

andrei.alekseev@protonmail.com.

#### 4. КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ.
Для сборки приложения используется фреймворк Apache Maven 3.6.0.
Команды для сборки приложения: 
````
mvn clean install
````

#### 5. ЗАПУСК ПРИЛОЖЕНИЯ.
Приложение запускается с помощью контейнера сервлетов Apache Tomcat.