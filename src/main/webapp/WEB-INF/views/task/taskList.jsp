<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
      </ol>
    </nav>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>[All tasks]</title>
</head>
<body>

<h2>Task Manager</h2>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">description</th>
      <th scope="col">created</th>
      <th scope="col">edit</th>
      <th scope="col">remove</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="task" items="${listOfAllTasks}">
            <tr>
                <td>${task.id}</td>
                <td>${task.name}</td>
                <td>${task.description}</td>
                <td>${task.createdOn}</td>
                <td><button type="button" class="btn btn-dark"><a href="/edit-task/${task.id}">click to edit</a></button></td>
                <td><button type="button" class="btn btn-dark"><a href="/delete-task/${task.id}">click to delete</a></button></td>
            </tr>
        </c:forEach>
  </tbody>
</table>
<c:url value="/add-task" var="add"/>
<button type="button" class="btn btn-outline-success"><a href="${add}">Create Task</a></button>
</body>
</html>