<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/task">Tasks</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Tasks</li>
      </ol>
    </nav>
<style>
        #create{
            margin-top:20px;
            margin-left:50px;
        }
        #form{
            margin-left:50px;
            width: 50%;
        }
        #status{
            margin-left:50px;
            width: 50%;
        }
        #button{
            margin-top:20px;
            margin-left:50px;
        }
        #button2{
            margin-top:20px;
        }
 </style>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <c:if test="${empty task.id}">
        <title>[Add task]</title>
    </c:if>
    <c:if test="${!empty task.id}">
        <title>[Update task]</title>
    </c:if>
</head>
<body>
<c:if test="${empty task.id}">
    <c:url value="/add-task" var="var"/>
</c:if>
<c:if test="${!empty task.id}">
    <c:url value="/edit-task" var="var"/>
</c:if>
<form action="${var}" method="POST">
    <c:if test="${!empty task.id}">
        <input type="hidden" name="id" value="${task.id}">
    </c:if>
        <div class="form-group">
             <label for="name">Task name</label>
             <input type="text" name="name" id="name">
        </div>
        <div class="form-group">
             <label for="description">Task description</label>
             <input type="text" name="description" id="description">
        </div>
        <div class="form-group">
             <label for="createdOn">Task creation date</label>
             <input type="date" name="createdOn" id="createdOn">
        </div>

    <c:if test="${empty task.id}">
        <input id="button" class="btn btn-outline-success" type="submit" value="Add new task">
    </c:if>
    <c:if test="${!empty task.id}">
        <input id="button" class="btn btn-outline-success" type="submit" value="Update task">
    </c:if>

    <c:url value="/task" var="task"/>
    <a id="button2" href="${task}" class="btn btn-outline-danger">Cancel</a>
</form>
</body>
</html>