package com.alekseev.tm.service;

import com.alekseev.tm.api.IProjectService;
import com.alekseev.tm.entity.Project;
import com.alekseev.tm.repository.IProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {
    @Autowired
    private IProjectRepository repository;

    @Transactional
    @Override
    public final void addOne(@Nullable final Project entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Transactional
    @Override
    public final void addAll(@Nullable final List<Project> entityList) {
        if (entityList == null) return;
        for (Project project : entityList)
            repository.save(project);
    }

    @Nullable
    @Transactional
    @Override
    public final Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.getOne(id);
    }

    @NotNull
    @Transactional
    @Override
    public final List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public final void update(@Nullable final Project entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Override
    @Transactional
    public final void deleteOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public final void deleteAll() {
        repository.deleteAll();
    }
}
