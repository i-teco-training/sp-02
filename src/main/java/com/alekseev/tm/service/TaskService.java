package com.alekseev.tm.service;

import com.alekseev.tm.api.ITaskService;
import com.alekseev.tm.entity.Task;
import com.alekseev.tm.repository.ITaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {
    @Autowired
    private ITaskRepository repository;

    @Override
    @Transactional
    public void addOne(Task entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Override
    @Transactional
    public void addAll(List<Task> entityList) {
        if (entityList == null) return;
        for (Task task : entityList)
            repository.save(task);
    }

    @Override
    @Transactional
    public Task findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.getOne(id);
    }

    @Override
    @Transactional
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void update(Task entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Override
    @Transactional
    public void deleteOne(String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }
}