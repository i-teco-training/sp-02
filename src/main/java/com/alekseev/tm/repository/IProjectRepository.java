package com.alekseev.tm.repository;

import com.alekseev.tm.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
    @Override
    List<Project> findAll();

    @Override
    Project getOne(String id);

    @Override
    <S extends Project> List<S> saveAll(Iterable<S> iterable);

    @Override
    <S extends Project> S save(S s);

    @Override
    void deleteById(String s);

    @Override
    void deleteAll();
}
