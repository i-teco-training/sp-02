package com.alekseev.tm.repository;

import com.alekseev.tm.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
    @Override
    List<Task> findAll();

    @Override
    <S extends Task> List<S> saveAll(Iterable<S> iterable);

    @Override
    Task getOne(String s);

    @Override
    <S extends Task> S save(S s);

    @Override
    void deleteById(String string);

    @Override
    void deleteAll();
}
