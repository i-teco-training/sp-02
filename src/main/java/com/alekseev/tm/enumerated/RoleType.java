package com.alekseev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum RoleType {
    USER("user"),
    ADMIN("administrator");

    RoleType() {}

    private String displayName;

    RoleType(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }

    @NotNull
    public static RoleType getRoleTypeFromString(@Nullable final String displayName){
        for (@NotNull final RoleType roleType: RoleType.values()) {
            if (roleType.toString().equals(displayName))
                return roleType;
        }
        return USER;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
