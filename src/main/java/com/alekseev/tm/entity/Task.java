package com.alekseev.tm.entity;

import com.alekseev.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "app_task")
public class Task extends AbstractEntity {
    public Task() {
    }

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status taskStatus = Status.PLANNED;

    @NotNull
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date createdOn = new Date();

    @Nullable
    @ManyToOne
    @JoinColumn(name = "projectId")
    private Project project;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @NotNull
    public Status getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(@NotNull Status taskStatus) {
        this.taskStatus = taskStatus;
    }

    @NotNull
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(@NotNull Date createdOn) {
        this.createdOn = createdOn;
    }

    @Nullable
    public Project getProject() {
        return project;
    }

    public void setProject(@Nullable Project project) {
        this.project = project;
    }
}
