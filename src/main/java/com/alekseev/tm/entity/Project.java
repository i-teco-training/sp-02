package com.alekseev.tm.entity;

import com.alekseev.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_project")
public class Project extends AbstractEntity {
    public Project() {
    }

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status projectStatus = Status.PLANNED;

    @NotNull
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date createdOn = new Date();

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE)
    private List<Task> tasks;

    @Nullable
    public String getName() {
        return name;
    }


    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @NotNull
    public Status getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(@NotNull Status projectStatus) {
        this.projectStatus = projectStatus;
    }

    @NotNull
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(@NotNull Date createdOn) {
        this.createdOn = createdOn;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }
}
