package com.alekseev.tm.api;

import com.alekseev.tm.entity.Project;
import com.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void addOne(Task entity);

    void addAll(List<Task> entityList);

    Task findOne(String id);

    List<Task> findAll();

    void update(Task entity);

    void deleteOne(String id);

    void deleteAll();
}
