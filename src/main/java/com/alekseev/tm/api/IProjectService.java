package com.alekseev.tm.api;

import com.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void addOne(Project entity);

    void addAll(List<Project> entityList);

    Project findOne(String id);

    List<Project> findAll();

    void update(Project entity);

    void deleteOne(String id);

    void deleteAll();
}
