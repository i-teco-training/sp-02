package com.alekseev.tm.controller;

import com.alekseev.tm.api.ITaskService;
import com.alekseev.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class TaskController {
    @Autowired
    private ITaskService taskService;

    @GetMapping(value = "/task")
    public final ModelAndView findAllTasks() {
        @Nullable final List<Task> allTasks = taskService.findAll();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task/taskList");
        modelAndView.addObject("listOfAllTasks", allTasks);
        return modelAndView;
    }

    @GetMapping(value = "/add-task")
    public final ModelAndView addPage() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task/taskEdit");
        return modelAndView;
    }

    @PostMapping(value = "/add-task")
    public final ModelAndView addTask(@ModelAttribute("task") Task task) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        taskService.addOne(task);
        return modelAndView;
    }

    @GetMapping(value = "/edit-task/{id}")
    public final ModelAndView editPage(@PathVariable("id") String id) {
        @Nullable final Task task = taskService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task/taskEdit");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @PostMapping(value = "/edit-task")
    public final ModelAndView updateTask(@ModelAttribute("task") Task task) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        taskService.update(task);
        return modelAndView;
    }

    @GetMapping(value="/delete-task/{id}")
    public final ModelAndView deleteTask(@PathVariable("id") String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        taskService.deleteOne(id);
        return modelAndView;
    }

    @InitBinder
    private void dataBinder(WebDataBinder binder){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        binder.registerCustomEditor(Date.class, editor);
    }
}
