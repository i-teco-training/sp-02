package com.alekseev.tm.controller;

import com.alekseev.tm.api.IProjectService;
import com.alekseev.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ProjectController {
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/project")
    public final ModelAndView findAllProjects() {
        //projectService.initProjects();
        @Nullable final List<Project> allProjects = projectService.findAll();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project/projectList");
        modelAndView.addObject("listOfAllProjects", allProjects);
        return modelAndView;
    }

    @GetMapping(value = "/add-project")
    public final ModelAndView addPage() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project/projectEdit");
        return modelAndView;
    }

    @PostMapping(value = "/add-project")
    public final ModelAndView addProject(@ModelAttribute("project") Project project) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        projectService.addOne(project);
        return modelAndView;
    }

    @GetMapping(value = "/edit-project/{id}")
    public final ModelAndView editPage(@PathVariable("id") String id) {
        @Nullable final Project project = projectService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project/projectEdit");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping(value = "/edit-project")
    public final ModelAndView updateProject(@ModelAttribute("project") Project project) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        projectService.update(project);
        return modelAndView;
    }

    @GetMapping(value="/delete-project/{id}")
    public final ModelAndView deleteProject(@PathVariable("id") String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        projectService.deleteOne(id);
        return modelAndView;
    }

    @InitBinder
    private void dataBinder(WebDataBinder binder){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        binder.registerCustomEditor(Date.class, editor);
    }

}
